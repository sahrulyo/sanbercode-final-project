import React from "react";
import  { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import Home from "./components/Home";
import Login from "./components/login";
import Dashboard from "./components/dashboard";
import Vacancy from "./components/vacancy";
import Footer from "./components/footer";
import SignUp from "./components/signup";
import JobVacancyForm from "./components/vacancy_input";
import Cookies from "js-cookie";
import Layout from "./components/layout";

//import { Link } from "react-router-dom";
//import './App.css';

const App = () => {
  const LoginRoute = ({ children }) => {
    if (Cookies.get('token') !== undefined) {
      return <Navigate to={'/'} />;
    } else {
      return children;
    }
  };

  const AuthRoute = ({ children }) => {
    if (Cookies.get('token') === undefined) {
      return <Navigate to={'/login'} />;
    } else {
      return children;
    }
  };

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/vacancy" element={<Vacancy />} />
        <Route path="/dashboard" element={<AuthRoute><Dashboard /></AuthRoute>} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/login" element={<LoginRoute><Layout><Login /></Layout></LoginRoute>} />
        <Route path="/footer" element={<Footer />} />
        <Route path="/jobvacancyform" element={<AuthRoute><JobVacancyForm /></AuthRoute>} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;