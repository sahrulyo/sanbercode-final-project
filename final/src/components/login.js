import React, { useState } from "react";
import Navbar from "./navbar";
import Footer from "./footer";
import axios from 'axios';
import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";

const Login = () => {
  
  let navigate = useNavigate()
  
  const [input, setInput] = useState ({
    email:"",
    password:"",
  
  });

  const handleInput = (event) => {
    let value = event.target.value
    let name = event.target.name

    setInput({...input, [name] : value})
  }

  const handleLogin = (event) => {
    event.preventDefault();

    let { email, password} = input
    console.log(input)

    axios.post('https://dev-example.sanbercloud.com/api/login ', {email, password} )
    .then((res) => {
        console.log(res)
        let data = res.data
        Cookies.set('token', data.token, {expires : 1})
        navigate('/')
        alert ('Berhasil login!')
    })

    .catch((error) => {
         console.log(error)
        alert(error.message)
    })

}
  return (
    <>
      <div>
        <Navbar/>
      </div>
      <form className="max-w-sm mx-auto h-96" onSubmit={handleLogin}>
        <div className="mb-5 mt-20">
          <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your email</label>
          <input value={input.email}  onChange={handleInput} type="email" name="email" id="email" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="your@email.com" required />
        </div>
        <div className="mb-5">
          <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your password</label>
          <input onChange={handleInput} value={input.password} type="password" name="password" id="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required />
        </div>
        
          <input type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" />
        
      </form>
      <Footer />
    </>
  );
};

export default Login;
