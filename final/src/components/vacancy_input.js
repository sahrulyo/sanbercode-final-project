import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Navbar from './navbar';
import Cookies from "js-cookie";

function JobVacancyForm() {
  const [data, setData] = useState(null);
  const [formData, setFormData] = useState({
    title: '',
    job_description: '',
    job_qualification: '',
    job_type: '',
    job_tenure: '',
    job_status: '',
    company_name: '',
    company_image_url: '',
    company_city: '',
    salary_min: '',
    salary_max: ''
  });
  const [editingId, setEditingId] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const result = await axios.get("https://dev-example.sanbercloud.com/api/job-vacancy", {
        headers: {
          Authorization: `Bearer ${Cookies.get("token")}`
        }
      });
      setData(result.data.data);
    } catch (error) {
      console.log("error fetching data:", error);
      setError("Failed to fetch data.");
    }
  };

  const handleInput = (event) => {
    const { name, value } = event.target;
    setFormData((prev) => ({ ...prev, [name]: value }));
  };

  const handleDelete = async (id) => {
    try {
      await axios.delete(`https://dev-example.sanbercloud.com/api/job-vacancy/${id}`, {
        headers: {
          Authorization: `Bearer ${Cookies.get("token")}` //ambil token dari localStorage
        }
      });
      fetchData();
    } catch (error) {
      console.error("Error deleting data:", error);
      setError("Failed to delete data.");
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      if (editingId) {
        await axios.put(`https://dev-example.sanbercloud.com/api/job-vacancy/${editingId}`, formData, {
          headers: {
            Authorization: `Bearer ${Cookies.get("token")}`
          }
        });
      } else {
        await axios.post("https://dev-example.sanbercloud.com/api/job-vacancy", formData, {
          headers: {
            Authorization: `Bearer ${Cookies.get("token")}`
          }
        });
      }
      fetchData();
      setFormData({
        title: '',
        job_description: '',
        job_qualification: '',
        job_type: '',
        job_tenure: '',
        job_status: '',
        company_name: '',
        company_image_url: '',
        company_city: '',
        salary_min: '',
        salary_max: ''
      });
      setEditingId(null);
    } catch (error) {
      console.error("Error adding/editing data:", error);
      setError("Failed to submit data.");
    }
  };

  const handleEdit = (id, index) => {
    const editedData = data[index];
    setFormData({ ...editedData });
    setEditingId(id);
  };


  return (
    <>
      <Navbar />
      <div>
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg" style={{ marginRight: "2%", marginLeft: "2%", marginTop: "10%", marginBottom: "10%" }}>
          {/* Tabel Data */}
          <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
            <thead className="text-xs text-gray-700 uppercase bg-violet-100 dark:bg-violet dark:text-gray-400">
              <tr>
                <th scope="col" className="p-4">
                  <div className="flex items-center">
                    <label htmlFor="checkbox-all-search" className="sr-only">checkbox</label>
                  </div>
                </th>
                <th scope="col" className="px-6 py-3">NO</th>
                <th scope="col" className="px-6 py-3">TITLE</th>
                <th scope="col" className="px-6 py-3">JOB DESCRIPTION</th>
                <th scope="col" className="px-6 py-3">JOB QUALIFICATION</th>
                <th scope="col" className="px-6 py-3">JOB TYPE</th>
                <th scope="col" className="px-6 py-3">JOB TENURE</th>
                <th scope="col" className="px-6 py-3">JOB STATUS</th>
                <th scope="col" className="px-6 py-3">COMPANY NAME</th>
                <th scope="col" className="px-6 py-3">COMPANY IMAGE URL</th>
                <th scope="col" className="px-6 py-3">COMPANY CITY</th>
                <th scope="col" className="px-6 py-3">SALARY MIN</th>
                <th scope="col" className="px-6 py-3">SALARY MAX</th>
                <th scope="col" className="px-6 py-3">ACTION</th>
              </tr>
            </thead>
            <tbody>
              {data && data.map((result, index) => (
                <tr key={result.id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                  <td className="w-4 p-4">
                    <div className="flex items-center">
                      <label htmlFor={`checkbox-table-search-${index + 1}`} className="sr-only">checkbox</label>
                    </div>
                  </td>
                  <td className="px-6 py-4">{index + 1}</td>
                  <td className="px-6 py-4">{result.title}</td>
                  <td className="px-6 py-4">{result.job_description}</td>
                  <td className="px-6 py-4">{result.job_qualification}</td>
                  <td className="px-6 py-4">{result.job_type}</td>
                  <td className="px-6 py-4">{result.job_tenure}</td>
                  <td className="px-6 py-4">{result.job_status}</td>
                  <td className="px-6 py-4">{result.company_name}</td>
                  <td className="px-6 py-4"><span className="px-6 py-4 overflow-hidden truncate w-[200px] ">{result.company_image_url}</span></td>
                  <td className="px-6 py-4">{result.company_city}</td>
                  <td className="px-6 py-4">{result.salary_min}</td>
                  <td className="px-6 py-4">{result.salary_max}</td>
                  <td className="px-6 py-4">
                    <button onClick={() => handleDelete(result.id)} className="font-medium text-red-600 dark:text-red-500 hover:underline ms-3">DELETE</button>
                    <button onClick={() => handleEdit(result.id, index)} className="font-medium text-blue-600 dark:text-blue-500 hover:underline ms-3">EDIT</button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        {/* Form Input */}
        <form onSubmit={handleSubmit}>
          <label>
            JOB TITLE:
            <input type="text" name="title" value={formData.title || ''} onChange={handleInput} required />
          </label>
          <label>
            JOB DESCRIPTION:
            <input type="text" name="job_description" value={formData.job_description || ''} onChange={handleInput} required />
          </label>
          <label>
            LOGO URL:
            <input type="text" name="company_image_url" value={formData.company_image_url || ''} onChange={handleInput} required />
          </label>
          <label>
            QUALIFICATION:
            <input type="text" name="job_qualification" value={formData.job_qualification || ''} onChange={handleInput} required />
          </label>
          <label>
            JOB TYPE:
            <input type="text" name="job_type" value={formData.job_type || ''} onChange={handleInput} required />
          </label>
          
          <label>
            JOB TENURE:
            <input type="text" name="job_tenure" value={formData.job_tenure || ''} onChange={handleInput} required />
          </label>
          <label>
            JOB STATUS:
            <input type="number" name="job_status" value={formData.job_status || ''} onChange={handleInput} required />
          </label>
          <label>
            COMPANY NAME:
            <input type="text" name="company_name" value={formData.company_name || ''} onChange={handleInput} required />
          </label>
          <label>
            CITY:
            <input type="text" name="company_city" value={formData.company_city || ''} onChange={handleInput} required />
          </label>
          <label>
            SALARY MIN:
            <input type="number" name="salary_min" value={formData.salary_min || ''} onChange={handleInput} required />
          </label>
          <label>
            SALARY MAX:
            <input type="number" name="salary_max" value={formData.salary_max || ''} onChange={handleInput} required />
          </label>
          {error && <p style={{ color: 'red' }}>{error}</p>}
          <button type="submit">Submit</button>
        </form>
      </div>
    </>
  );
}

export default JobVacancyForm;
